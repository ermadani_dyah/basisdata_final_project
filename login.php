<?php
    session_start();
    include_once('Admin_perpustakan/koneksi/koneksi.php');
    require('Admin_perpustakan/oop/db_anggota.php');
    require('Admin_perpustakan/oop/db_buku.php');
    $obj = new Db_Anggota();
    $obj1 = new Db_Buku();
    if(isset($_GET['data'])){
        $id_buku = $_GET['data'];
        $_SESSION['id_buku']=$id_buku;
        
        if(isset($_POST['login'])&&!empty($_POST['login'])){
            if ((isset($_POST["username"]))&&(isset($_POST["password"]))){
                $username = $_POST["username"];
                $password = $_POST["password"];
                $_SESSION['username'] = $username;
                $_SESSION['password'] = $password;
                $sql=$obj->login($username,$password);
                $_SESSION['login'] = pg_fetch_object($sql);
                $login_check = pg_NumRows($sql);
                for ($j=0; $j<$login_check; $j++){
                    $id= pg_result($sql, $j, "id_anggota");
                }
                
                if($login_check > 0){ 
                    echo '<script type="text/javascript">'; 
                    echo 'alert("Yee Anda Berhasil Login");'; 
                    echo 'window.location.href = "pinjam.php";';
                    echo '</script>';    
                }else{
                    echo '<script type="text/javascript">'; 
                    echo 'alert("Anda Gagal Login");'; 
                    echo 'window.location.href = "index.php";';
                    echo '</script>';  
                }
            }
        }
    }
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head_login.php");?>
	</head>
    <div class="container-fluid">
        <div class="row main-content bg-success text-center">
            <div class="col-md-4 text-center company__info" style="text-align: center;">
                <span class="company__logo">
                    <img src="img/simple-house-logo.png" alt="">
                </span>
                <h4 class="company_title">The Reading Room</h4>
            </div>
            <div class="col-md-8 col-xs-12 col-sm-12 login_form ">
                <div class="container-fluid">
                    <div class="row" style="text-align: center;">
                        <h2>Log In</h2>
                    </div>
                    <div class="row">
                        <form control="" class="form-group" method="post">
                            <div class="row">
                                <input type="text" name="username" id="username" class="form__input" placeholder="Username">
                            </div>
                            <div class="row">
                                <!-- <span class="fa fa-lock"></span> -->
                                <input type="password" name="password" id="password" class="form__input" placeholder="Password">
                            </div>
                            <div class="row" style="text-align: center;">
                                <button type="submit" name="login" value="Submit" class="btn">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</html>