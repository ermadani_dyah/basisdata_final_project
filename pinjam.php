<?php 
session_start();
include_once('Admin_perpustakan/koneksi/koneksi.php');
require('Admin_perpustakan/oop/db_anggota.php');
require('Admin_perpustakan/oop/db_buku.php');
require('Admin_perpustakan/oop/db_petugas.php');
require('Admin_perpustakan/oop/db_pinjaman.php');
$obj = new Db_Anggota();
$obj1 = new Db_Buku();
$obj2 = new Db_Petugas();
$obj3 = new Db_Pinjaman();
$login = $_SESSION['login'];
$id = $_SESSION['id_buku'];
$data=$obj1->getBukuKode($id);
$petugas = $obj2->getPetugas();
$check = pg_NumRows($data);
for ($j=0; $j<$check; $j++){
    $id_buku= pg_result($data, $j, "id_buku");
    $nama= pg_result($data, $j, "judul");
}
if(isset($_POST['submit']) and !empty($_POST['submit'])){
    $ret_val = $obj3->createPinjaman();
    $transaksi = $obj3->getPinjamanById();
    $_SESSION['transaksi'] = pg_fetch_object($transaksi);
    if($ret_val==1){
        echo '<script type="text/javascript">'; 
        echo 'alert("Record Saved Successfully");'; 
        echo 'window.location.href = "detail_transaksi.php";';
        echo '</script>';
    }
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">The Reading Room</h2>
                    <p class="col-12 text-center">Silahkan Lakukan Penyewaan Anda</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form h-100">
                                <h3>Get Started</h3>
                                <form class="mb-5" method="post" id="contactForm" name="contactForm">
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Id Transaksi *</label>
                                            <input type="text" class="form-control" name="id_pinjaman" id="id_pinjaman"  value="">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Id Anggota *</label>
                                            <input type="text" class="form-control" name="anggota" id="anggota"  value="<?=$login->id_anggota?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Name Anggota *</label>
                                            <input type="text" class="form-control" name="nama_anggota" id="nama_anggota"  value="<?=$login->nama?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Nama Buku *</label>
                                            <input type="text" class="form-control" name="buku" id="buku" placeholder="<?=$nama?>" value="<?=$id_buku?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Petugas *</label>
                                            <div class="col-sm-7">
                                                <select class="form-control" id="petugas" name="petugas">
                                                    <option value="0">- Pilih Petugas -</option>
                                                    <?php while($ptg = pg_fetch_object($petugas)): 
                                                        $id=$ptg->id_petugas;
                                                        $nama=$ptg->nama_petugas;
                                                    
                                                    ?>  
                                                    <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_petugas'])){if($id==$_SESSION['nama_petugas']){?> selected="selected" <?php }}?>>
                                                        <?php echo $nama;?>
                                                    <?php endwhile; ?>
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Tanggal Pinjam *</label>
                                            <input type="date" class="form-control" name="tanggal_pinjaman" id="tanggal_pinjaman" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <button type="submit" class="tm-btn tm-btn-default tm-right" name="submit"  value="Submit">
                                                Pinjam
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>