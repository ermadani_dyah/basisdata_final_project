<?php 
session_start();
include_once('Admin_perpustakan/koneksi/koneksi.php');
require('Admin_perpustakan/oop/db_pinjaman.php');
$obj = new Db_Pinjaman();
$transaksi = $_SESSION['transaksi'];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">TRANSAKSI ANDA</h2>
                    <p class="col-12 text-center">Terimakasih Sudah Melakukan Transaksi</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="mb-5"> 
                        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <i class="fas fa-user-circle"></i> 
                                        <strong>Detail Transaksi</strong>
                                    </td>
                                </tr>  
                                <tr>
                                    <td width="20%">
                                        <strong>Id Transaksi:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->id_pinjaman;?>
                                    </td>
                                </tr>             
                                <tr>
                                    <td width="20%">
                                        <strong>Nama Anggota:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->nama;?>
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Buku:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->judul;?>
                                    </td>
                                </tr>                 
                                <tr>
                                    <td width="20%">
                                        <strong>Petugas:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->nama_petugas;?>
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Pinjaman:   <strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->tanggal_pinjaman;?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Kembali:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->tanggal_kembali;?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        <strong>Status:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $transaksi->status_pinjaman;?>
                                    </td>
                                </tr>
                            </tbody>
                        </table> 
                        <hr>
                        <h3>Terimakasih</h3>
                        <p>
                            Transaksi persewaan anda telah berhasil<br>
                            Semoga Hari Anda Menyenangkan dan Teruslah Membaca Buku: <br>
                            <strong>Jangan Lupa Untuk Pengembaliannya</strong> Jangan Sampai Jatuh Tempo
                        </p>
                        <p>status akan berubah dalam 1x24 jam</p>
                        <br>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <a href="profil.php?data=<?php echo $transaksi->id_pinjaman?>" class="tm-btn tm-btn-default tm-right">Konfirmasi</a>
                            </div>
                        </div>
                    </div>
                </div>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>