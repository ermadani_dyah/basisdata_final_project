<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"> The Reading Room</span>
    </a>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="dashboard.php" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="rak_buku.php" class="nav-link">
                <i class="nav-icon fas fa-swatchbook"></i>
                <p>Rak Buku</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="jenis_buku.php" class="nav-link">
                <i class="nav-icon fas fa-list-ul"></i>
                <p>Jenis Buku</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="penulis.php" class="nav-link">
                <i class="nav-icon fas fa-feather-alt"></i>
                <p>Penulis</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="buku.php" class="nav-link">
                <i class="nav-icon fas fa-book-reader"></i>
                <p>Buku</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="anggota.php" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>Anggota</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="petugas.php" class="nav-link">
                <i class="nav-icon fas fa-user-tie"></i>
                <p>Petugas</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="pinjaman.php" class="nav-link">
                <i class="nav-icon far fa-calendar-minus"></i>
                <p>Pinjaman</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="pengembalian.php" class="nav-link">
                <i class="nav-icon far fa-calendar-plus"></i>
                <p>Pengembalian</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="index.php" class="nav-link">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</aside>
<!-- /.sidebar -->