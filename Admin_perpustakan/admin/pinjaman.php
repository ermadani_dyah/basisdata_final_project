<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_pinjaman.php');
$obj = new Db_Pinjaman();
$pinjaman = $obj->getPinjaman();
$sn=1;
if(isset($_POST['delete'])){
   $ret_val = $obj->deletePinjaman();
   if($ret_val==1){
       
      echo "<script language='javascript'>";
      echo "alert('Record Deleted Successfully'){
          window.location.reload();
      }";
      echo "</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Preloader -->
            

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0">Pinjaman</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Pinjaman</li>
                        </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="card-header">
                                                <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="mr-3 fas fa-clipboard-list"></i>Daftar Pinjaman
                                                </h3>
                                                <div class="card-tools">
                                                    <a href="tambah_pinjaman.php" class="btn btn-sm btn-info float-right">
                                                        <i class="fas fa-plus"></i> Tambah Daftar Pinjaman
                                                    </a>
                                                </div>
                                            </div> 
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <form method="post" action="search_pinjaman.php">
                                                        <div class="row">
                                                            <div class="col-md-4 bottom-10">
                                                                <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Kode Pinjaman">
                                                            </div>
                                                            <div class="col-md-5 bottom-10">
                                                                <input type="submit" name="search" value="Search" class="btn btn-primary"/>
                                                            </div>
                                                        </div>
                                                        <!-- .row -->
                                                    </form>
                                                </div>
                                                <br>
                                                <div class="col-sm-12">
                                                    <!--Pesan Gagal-->
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                        <tr>
                                                            <th width="5%">NO</th>
                                                            <th width="15%">Id Pinjaman</th>
                                                            <th width="15%">Anggota</th>
                                                            <th width="15%">Buku</th>
                                                            <th width="15%">Petugas</th>
                                                            <th width="15%">Tanggal Pinjaman</th>
                                                            <th width="15%">Tanggal Kembali</th>
                                                            <th width="15%">Status</th>
                                                            <th width="20%">
                                                                <center>Aksi</center>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php while($pin = pg_fetch_object($pinjaman)):?>  	
                                                        <?php
                                                            $id=$pin->id_pinjaman;
                                                            $kembali=$pin->tanggal_kembali;
                                                            $now = date('Y-m-d');
                                                            $masaberlaku = strtotime($now) - strtotime($kembali);
                                                            $waktu=$masaberlaku/(24*60*60);
                                                        ?>
                                                        <tr>
                                                            <td><?=$sn++?></td>
                                                            <td><?= $id?></td>
                                                            <td><?=$pin->nama?></td>
                                                            <td><?=$pin->judul?></td>
                                                            <td><?=$pin->nama_petugas?></td>
                                                            <td><?=$pin->tanggal_pinjaman?></td>
                                                            <td><?=$pin->tanggal_kembali?></td>
                                                            <td> 
                                                                <?php 
                                                                    if($waktu<1){
                                                                        $ret_val = $obj->getUpdateStatus1($id);
                                                                        echo "Dipinjam";
                                                                    }
                                                                    else if($waktu<7){
                                                                        $ret_val = $obj->getUpdateStatus2($id);
                                                                        echo 'Terlambat';
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td align="center">
                                                                <form method="post"> 
                                                                    <input type="submit" onClick="return confirm('Please confirm deletion');" class="btn btn-danger" name= "delete" value="Delete">
                                                                    <input type="hidden" value="<?=$id?>" name="id_pinjaman">
                                                                </form>  
                                                            </td>
                                                        </tr>
                                                        <?php endwhile; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <span>Showing  to  of  entries</span>
                                                <ul class="pagination pagination-sm m-0 float-right">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>