<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_kembali.php');
$obj = new Db_Kembali();
$kembali = $obj->getKembali();
$sn=1;
if(isset($_POST['update'])){
    $kembali = $obj->getKembaliById();
    $_SESSION['kembali'] = pg_fetch_object($kembali);
    $kembali2 = $_SESSION['kembali'];
    if(isset($_POST['update']) and !empty($_POST['update'])){
        $ret_val = $obj->updateKembali();
        if($ret_val==1){
            echo '<script type="text/javascript">'; 
            echo 'alert("Data Berhasil Di Konfirmasi");'; 
            echo 'window.location.href = "pengembalian.php";';
            echo '</script>';
        }
    }
}
if(isset($_POST['delete'])){
   $ret_val = $obj->deleteKembali();
   if($ret_val==1){
       
      echo "<script language='javascript'>";
      echo "alert('Record Deleted Successfully'){
          window.location.reload();
      }";
      echo "</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Preloader -->
            

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0">Pengembalian</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Pengembalian</li>
                        </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="card-header">
                                                <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="mr-3 fas fa-clipboard-list"></i>Daftar Pengembalian
                                                </h3>
                                                <div class="card-tools">
                                                    <a href="tambah_kembali.php" class="btn btn-sm btn-info float-right">
                                                        <i class="fas fa-plus"></i> Tambah Daftar Pengembalian
                                                    </a>
                                                </div>
                                            </div> 
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <form method="post" action="search_kembali.php">
                                                        <div class="row">
                                                            <div class="col-md-4 bottom-10">
                                                                <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Kode Kembali">
                                                            </div>
                                                            <div class="col-md-5 bottom-10">
                                                                <input type="submit" name="search" value="Search" class="btn btn-primary"/>
                                                            </div>
                                                        </div>
                                                        <!-- .row -->
                                                    </form>
                                                </div>
                                                <br>
                                                <div class="col-sm-12">
                                                    <!--Pesan Gagal-->
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                        <tr>
                                                            <th width="5%">NO</th>
                                                            <th width="15%">Id Kembali</th>
                                                            <th width="15%">Id Pinjaman</th>
                                                            <th width="15%">Petugas</th>
                                                            <th width="15%">Tanggal Pinjam</th>
                                                            <th width="15%">Tanggal Jatuh Tempo</th>
                                                            <th width="15%">Tanggal Kembali</th>
                                                            <th width="15%">Denda/Hari</th>
                                                            <th width="15%">Status</th>
                                                            <th width="20%">
                                                                <center>Aksi</center>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php while($kbl = pg_fetch_object($kembali)):?>  	
                                                        <?php
                                                            $id=$kbl->id_kembali;
                                                            $balik=$kbl->tgl_kembali;
                                                            $pinjam=$kbl->tanggal_pinjaman;
                                                            $kembalili=$kbl->tanggal_kembali;
                                                            $masaberlaku = strtotime($balik) - strtotime($kembalili) ;
                                                            $waktu=$masaberlaku/(24*60*60);
                                                            $denda=$waktu*5000;
                                                        ?>
                                                        <tr>
                                                            <td><?=$sn++?></td>
                                                            <td><?= $id;?></td>
                                                            <td><?= $kbl->id_pinjaman?></td>
                                                            <td><?=$kbl->nama_petugas?></td>
                                                            <td><?=$pinjam?></td>
                                                            <td><?=$kembalili?></td>
                                                            <td><?=$balik?></td>
                                                            <td>
                                                                <?php
                                                                    $ret_val = $obj->getUpdateStatus1($id,$denda);
                                                                    echo $denda;
                                                                ?>
                                                            </td>
                                                            <td><?=$kbl->status_kembali?></td>
                                                            <td align="center">
                                                                <form method="post">
                                                                    <input type="submit" class="btn btn-success" name= "update" value="Konfirmasi">   
                                                                    <input type="submit" onClick="return confirm('Please confirm deletion');" class="btn btn-danger" name= "delete" value="Delete">
                                                                    <input type="hidden" value="<?=$id?>" name="id_kembali">
                                                                </form>  
                                                            </td>
                                                        </tr>
                                                        <?php endwhile; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <span>Showing  to  of  entries</span>
                                                <ul class="pagination pagination-sm m-0 float-right">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>