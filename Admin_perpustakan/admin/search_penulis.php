<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_penulis.php');
$obj = new Db_Penulis();
if (isset($_POST["katakunci"])){
    $katakunci = $_POST["katakunci"];
    $_SESSION['katakunci'] = $katakunci;
    $sql=$obj->searchPenulis($katakunci);
    $rows = pg_NumRows($sql);
}
if(isset($_POST['update'])){
    $penulis = $obj->getPenulisById();
    $_SESSION['penulis'] = pg_fetch_object($penulis);
    header('location:edit_penulis.php');
}
if(isset($_POST['delete'])){
   $ret_val = $obj->deletePenulis();
   if($ret_val==1){
       
      echo "<script language='javascript'>";
      echo "alert('Data Berhasil Dihapus'){
          window.location.reload();
      }";
      echo "</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0">Penulis</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Penulis</li>
                        </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="card-header">
                                                <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="mr-3 fas fa-clipboard-list"></i>Daftar Penulis
                                                </h3>
                                                <div class="card-tools">
                                                    <a href="tambah_penulis.php" class="btn btn-sm btn-info float-right">
                                                        <i class="fas fa-plus"></i> Tambah Daftar Penulis
                                                    </a>
                                                </div>
                                            </div> 
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <form method="post" action="penulis.php">
                                                        <div class="row">
                                                            <div class="col-md-4 bottom-10">
                                                                <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Id Penulis dan Nama">
                                                            </div>
                                                            <div class="col-md-5 bottom-10">
                                                                <input type="submit" name="search" value="Search" class="btn btn-primary"/>
                                                            </div>
                                                        </div>
                                                        <!-- .row -->
                                                    </form>
                                                </div>
                                                <br>
                                                <div class="col-sm-12">
                                                    <!--Pesan Gagal-->
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                        <tr>
                                                            <th width="5%">NO</th>
                                                            <th width="15%">Id Penulis</th>
                                                            <th width="15%">Nama</th>
                                                            <th width="15%">Alamat</th>
                                                            <th width="15%">No Telfon</th>
                                                            <th width="15%">Email</th>
                                                            <th width="20%">
                                                                <center>Aksi</center>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sn=1;
                                                            for ($j=0; $j < $rows; $j++){
                                                                    $id= pg_result($sql, $j, "id_penulis");
                                                                    $nama = pg_result($sql, $j, "nama");
                                                                    $no = pg_result($sql, $j, "no_telfon"); 
                                                                    $alamat = pg_result($sql, $j, "alamat"); 
                                                                    $email = pg_result($sql, $j, "email");
                                                        ?>  	
                                                            <tr>
                                                                <td><?=$sn++?></td>
                                                                <td><?=$id?></td>
                                                                <td><?=$nama?></td>
                                                                <td><?=$alamat?></td>
                                                                <td><?=$no?></td>
                                                                <td><?=$email?></td>
                                                                <td align="center">
                                                                    <form method="post">
                                                                        <input type="submit" class="btn btn-success" name= "update" value="Update">   
                                                                        <input type="submit" onClick="return confirm('Apakah Anda Yakin Ingin Menghapus data ini?');" class="btn btn-danger" name= "delete" value="Delete">
                                                                        <input type="hidden" value="<?=$id?>" name="id_penulis">
                                                                    </form>  
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <span>Showing  to  of  entries</span>
                                                <ul class="pagination pagination-sm m-0 float-right">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>