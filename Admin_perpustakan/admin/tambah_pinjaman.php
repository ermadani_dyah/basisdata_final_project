<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_buku.php');
require('../oop/db_anggota.php');
require('../oop/db_petugas.php');
require('../oop/db_pinjaman.php');
$obj = new Db_Buku();
$obj1 = new Db_Anggota();
$obj2 = new Db_Petugas();
$obj3 = new Db_Pinjaman();
$anggota = $obj1->getAnggota();
$buku = $obj->getBuku();
$petugas = $obj2->getPetugas();
if(isset($_POST['submit']) and !empty($_POST['submit'])){
$ret_val = $obj3->createPinjaman();
if($ret_val==1){
    echo '<script type="text/javascript">'; 
    echo 'alert("Record Saved Successfully");'; 
    echo 'window.location.href = "pinjaman.php";';
    echo '</script>';
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Tambah Data Pinjaman</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="pinjaman.php">Pinjaman</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data pinjaman</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <section class="content">
                                                <div class="card card-info">
                                                    <div class="card-header">
                                                        <h3 class="card-title" style="margin-top:5px;">
                                                            <i class="mr-3 fas fa-plus-square"></i> Form Tambah Data Pinjaman
                                                        </h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <!-- form start -->
                                                    <br>
                                                    <div class="col-sm-10">
                                                        <!--Pesan Gagal-->
                                                    </div>
                                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" >
                                                        <div class="card-body">
                                                            <div class="form-group row">
                                                                <label for="kode" class="col-sm-3 col-form-label">Id Pinjaman <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="id_pinjaman" class="form-control" id="id_pinjaman" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Anggota <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="anggota" name="anggota">
                                                                        <option value="0">- Pilih anggota -</option>
                                                                        <?php while($agt = pg_fetch_object($anggota)): 
                                                                            $id=$agt->id_anggota;
                                                                            $nama=$agt->nama;
                                                                        
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_anggota'])){if($id==$_SESSION['nama_anggota']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $nama;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Buku <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="buku" name="buku">
                                                                        <option value="0">- Pilih Buku -</option>
                                                                        <?php while($bk = pg_fetch_object($buku)): 
                                                                            $id=$bk->id_buku;
                                                                            $judul=$bk->judul;
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_buku'])){if($id==$_SESSION['nama_buku']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $judul;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Petugas <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="petugas" name="petugas">
                                                                        <option value="0">- Pilih Petugas -</option>
                                                                        <?php while($ptg = pg_fetch_object($petugas)): 
                                                                            $id=$ptg->id_petugas;
                                                                            $nama=$ptg->nama_petugas;
                                                                        
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_petugas'])){if($id==$_SESSION['nama_petugas']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $nama;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nama" class="col-sm-3 col-form-label">Tanggal Pinjam <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="date" name="tanggal_pinjaman" class="form-control" id="tanggal_pinjaman" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-body -->
                                                        <div class="card-footer">
                                                            <button type="submit" class="btn btn-info float-right" name="submit"  value="Submit">
                                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                                            </button>
                                                            <div class="text-left upgrade-btn">
                                                                <a href="pinjaman.php" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                                    Kembali
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-footer -->
                                                    </form>
                                                </div>
                                                <!-- /.card -->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>