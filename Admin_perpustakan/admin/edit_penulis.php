<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_penulis.php');
$obj = new Db_Penulis();
$penulis = $_SESSION['penulis'];
if(isset($_POST['update']) and !empty($_POST['update'])){
    $ret_val = $obj->updatePenulis();
    if($ret_val==1){
        echo '<script type="text/javascript">'; 
        echo 'alert("Data Berhasil Di Edit");'; 
        echo 'window.location.href = "penulis.php";';
        echo '</script>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Edit Data Penulis</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="penulis.php">Penulis</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Penulis</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <section class="content">
                                                <div class="card card-info">
                                                    <div class="card-header">
                                                        <h3 class="card-title" style="margin-top:5px;">
                                                            <i class="mr-3 fas fa-edit"></i>Form Edit Data Penulis
                                                        </h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <!-- form start -->
                                                    <br>
                                                    <div class="col-sm-10">
                                                        <!--Pesan Gagal-->
                                                    </div>
                                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" >
                                                        <div class="card-body">
                                                            <div class="form-group row">
                                                                <label for="kode" class="col-sm-3 col-form-label">Id Penulis<span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="id_penulis" class="form-control" id="id_penulis" value= "<?=$penulis->id_penulis?>" required >
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nama" class="col-sm-3 col-form-label">Nama <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="nama_penulis" class="form-control" id="nama_penulis" value= "<?=$penulis->nama?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">No Telfon <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="no_penulis" class="form-control" id="no_penulis" value= "<?=$penulis->no_telfon?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Email <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="email_penulis" class="form-control" id="email_penulis" value= "<?=$penulis->email?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Alamat <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="alamat_penulis" class="form-control" id="alamat_penulis" value= "<?=$penulis->alamat?>" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-body -->
                                                        <div class="card-footer">
                                                            <button type="submit" class="btn btn-info float-right" name="update" value="Update">
                                                                <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                                            </button>
                                                            <div class="text-left upgrade-btn">
                                                                <a href="penulis.php" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                                    Kembali
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-footer -->
                                                    </form>
                                                </div>
                                                <!-- /.card -->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>