<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_anggota.php');
$obj = new Db_Anggota();
$anggota = $obj->getAnggota();
$sn=1;
if(isset($_POST['update'])){
    $anggota = $obj->getAnggotaById();
    $_SESSION['anggota'] = pg_fetch_object($anggota);
    header('location:edit_anggota.php');
}
if(isset($_POST['detail'])){
    $anggota = $obj->getAnggotaById();
    $_SESSION['anggota'] = pg_fetch_object($anggota);
    header('location:detail_anggota.php');
}
if(isset($_POST['delete'])){
   $ret_val = $obj->deleteAnggota();
   if($ret_val==1){
       
      echo "<script language='javascript'>";
      echo "alert('Data Berhasil Dihapus'){
          window.location.reload();
      }";
      echo "</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0">Anggota</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Anggota</li>
                        </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="card-header">
                                                <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="mr-3 fas fa-clipboard-list"></i>Daftar Anggota
                                                </h3>
                                                <div class="card-tools">
                                                    <a href="tambah_anggota.php" class="btn btn-sm btn-info float-right">
                                                        <i class="fas fa-plus"></i> Tambah Daftar Anggota
                                                    </a>
                                                </div>
                                            </div> 
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <form method="post" action="search_anggota.php">
                                                        <div class="row">
                                                            <div class="col-md-4 bottom-10">
                                                                <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Id Anggota dan Nama">
                                                            </div>
                                                            <div class="col-md-5 bottom-10">
                                                                <input type="submit" name="search" value="Search" class="btn btn-primary"/>
                                                            </div>
                                                        </div>
                                                        <!-- .row -->
                                                    </form>
                                                </div>
                                                <br>
                                                <div class="col-sm-12">
                                                    <!--Pesan Gagal-->
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                        <tr>
                                                            <th width="5%">NO</th>
                                                            <th width="15%">Id Anggota</th>
                                                            <th width="15%">Nama</th>
                                                            <th width="20%">
                                                                <center>Aksi</center>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php while($agt= pg_fetch_object($anggota)): ?>  	
                                                        <tr>
                                                            <td><?=$sn++?></td>
                                                            <td><?=$agt->id_anggota?></td>
                                                            <td><?=$agt->nama?></td>
                                                            <td align="center">
                                                                <form method="post">
                                                                    <input type="submit" class="btn btn-success" name= "update" value="Update">   
                                                                    <input type="submit" onClick="return confirm('Anda Yakin Akan Menghapus ini?');" class="btn btn-danger" name= "delete" value="Delete">
                                                                    <input type="submit" class="btn btn-info" name= "detail" value="Detail">
                                                                    <input type="hidden" value="<?=$agt->id_anggota?>" name="id_anggota">
                                                                </form>  
                                                            </td>
                                                        </tr>
                                                        <?php endwhile; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <span>Showing  to  of  entries</span>
                                                <ul class="pagination pagination-sm m-0 float-right">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>