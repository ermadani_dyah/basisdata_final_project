<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_buku.php');
require('../oop/db_rak_buku.php');
require('../oop/db_penulis.php');
require('../oop/db_jenis_buku.php');
$obj = new Db_Buku();
$obj1 = new Db_RakBuku();
$obj2 = new Db_Penulis();
$obj3 = new Db_JenisBuku();
$rak_buku = $obj1->getRakBuku();
$penulis = $obj2->getPenulis();
$jenis = $obj3->getJenisBuku();
$buku = $_SESSION['buku'];

?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Detail Data Buku</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="buku.php">Buku</a>
                                    </li>
                                    <li class="breadcrumb-item active">Detail Data Buku</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <section class="content">
                                                <div class="card card-info">
                                                    <div class="card-header">
                                                        <h3 class="card-title" style="margin-top:5px;">
                                                                <i class="mr-3 far fa-eye"></i>Form Detail Data Buku
                                                        </h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <!-- form start -->
                                                    <br>
                                                    <div class="card-body">
                                                        <table class="table table-bordered">
                                                            <tbody>  
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <i class="mr-3 fas fa-book"></i>
                                                                        <strong>Detail Buku</strong>
                                                                    </td>
                                                                </tr>               
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Judul</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->judul;?>
                                                                    </td>
                                                                </tr>                 
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Penerbit</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->penerbit?>
                                                                    </td>
                                                                </tr>                 
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Tahun Terbit</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->tahun_terbit?>
                                                                    </td>
                                                                </tr> 
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Jenis Buku</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->nama_jenis?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Rak Buku</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->id_rak?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Penulis</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <?php echo $buku->nama?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="20%">
                                                                        <strong>Gambar</strong>
                                                                    </td>
                                                                    <td width="80%">
                                                                        <img src="../dist/img/<?php echo $buku->gambar;?>" class="img-fluid" width="200px;" height="250px;">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.card-body -->
                                                    <div class="card-footer clearfix">
                                                        <div class="text-right upgrade-btn">
                                                            <a href="buku.php" class="btn btn-sm btn-info d-none d-md-inline-block text-white">
                                                                <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                                Kembali
                                                            </a>
                                                        </div>
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <!-- /.card -->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>