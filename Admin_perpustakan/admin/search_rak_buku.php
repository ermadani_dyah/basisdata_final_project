<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_rak_buku.php');
$obj = new Db_RakBuku();
if (isset($_POST["katakunci"])){
    $katakunci = $_POST["katakunci"];
    $_SESSION['katakunci'] = $katakunci;
    $sql=$obj->searchRakBuku($katakunci);
    
    $rows = pg_NumRows($sql);
}
if(isset($_POST['update'])){
    $rak_buku = $obj->getRakBukuById();
    $_SESSION['rak_buku'] = pg_fetch_object($rak_buku);
    header('location:edit_rak_buku.php');
}

if(isset($_POST['delete'])){
   $ret_val = $obj->deleteRakBuku();
   if($ret_val==1){
       
      echo "<script language='javascript'>";
      echo "alert('Data Berhasil Dihapus'){
          window.location.reload();
      }";
      echo "</script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Preloader -->
            

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        <h1 class="m-0">Rak Buku</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                            <a href="index.php">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Rak Buku</li>
                        </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <div class="card-header">
                                                <h3 class="card-title" style="margin-top:5px;">
                                                    <i class="mr-3 fas fa-clipboard-list"></i>Daftar Rak Buku
                                                </h3>
                                                <div class="card-tools">
                                                    <a href="tambah_rak_buku.php" class="btn btn-sm btn-info float-right">
                                                        <i class="fas fa-plus"></i> Tambah Daftar Rak Buku
                                                    </a>
                                                </div>
                                            </div> 
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <form method="post" action="rak_buku.php">
                                                        <div class="row">
                                                            <div class="col-md-4 bottom-10">
                                                                <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama Dosen">
                                                            </div>
                                                            <div class="col-md-5 bottom-10">
                                                                <input type="submit" name="Submit" value="Search" class="btn btn-primary"/>
                                                            </div>
                                                        </div>
                                                        <!-- .row -->
                                                    </form>
                                                </div>
                                                <br>
                                                <div class="col-sm-12">
                                                    <!--Pesan Gagal-->
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>                  
                                                        <tr>
                                                            <th width="5%">NO</th>
                                                            <th width="15%">Id Rak Buku</th>
                                                            <th width="15%">Kapasitas</th>
                                                            <th width="15%">Lokasi</th>
                                                            <th width="20%">
                                                                <center>Aksi</center>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                        $sn=1;
                                                        for ($j=0; $j < $rows; $j++){
                                                                $id= pg_result($sql, $j, "id_rak");
                                                                $kapasitas = pg_result($sql, $j, "kapasitas");
                                                                $lokasi = pg_result($sql, $j, "lokasi"); ?>  	
                                                        <tr>
                                                            <td><?=$sn++?></td>
                                                            <td><?=$id?></td>
                                                            <td><?=$kapasitas?></td>
                                                            <td><?=$lokasi?></td>
                                                            <td align="center">
                                                                <form method="post">
                                                                    <input type="submit" class="btn btn-success" name= "update" value="Update">   
                                                                    <input type="submit" onClick="return confirm('Apakah Anda Yakin Ingin Menghapus data ini?');" class="btn btn-danger" name= "delete" value="Delete">
                                                                    <input type="hidden" value="<?=$id?>" name="id_rak_buku">
                                                                </form>  
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <span>Showing  to  of  entries</span>
                                                <ul class="pagination pagination-sm m-0 float-right">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>