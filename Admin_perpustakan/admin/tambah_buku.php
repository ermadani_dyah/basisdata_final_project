<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_buku.php');
require('../oop/db_rak_buku.php');
require('../oop/db_penulis.php');
require('../oop/db_jenis_buku.php');
$obj = new Db_Buku();
$obj1 = new Db_RakBuku();
$obj2 = new Db_Penulis();
$obj3 = new Db_JenisBuku();
$rak_buku = $obj1->getRakBuku();
$penulis = $obj2->getPenulis();
$jenis = $obj3->getJenisBuku();
if(isset($_POST['submit']) and !empty($_POST['submit'])){
$lokasi_file = $_FILES['foto']['tmp_name'];
$ret_val = $obj->createBuku($lokasi_file);
if($ret_val==1){
    echo '<script type="text/javascript">'; 
    echo 'alert("Record Saved Successfully");'; 
    echo 'window.location.href = "buku.php";';
    echo '</script>';
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php include("../includes/head.php")?>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            

            <!-- Navbar -->
            <?php include("../includes/navbar.php")?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include("../includes/sidebar.php")?>
            <!-- /.sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Tambah Data Buku</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">
                                        <a href="index.php">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="buku.php">Buku</a>
                                    </li>
                                    <li class="breadcrumb-item active">Tambah Data Buku</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-sm-12">
                                        <div class="card-body">
                                            <section class="content">
                                                <div class="card card-info">
                                                    <div class="card-header">
                                                        <h3 class="card-title" style="margin-top:5px;">
                                                            <i class="mr-3 fas fa-plus-square"></i> Form Tambah Data Buku
                                                        </h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <!-- form start -->
                                                    <br>
                                                    <div class="col-sm-10">
                                                        <!--Pesan Gagal-->
                                                    </div>
                                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" >
                                                        <div class="card-body">
                                                            <div class="form-group row">
                                                                <label for="kode" class="col-sm-3 col-form-label">Id Buku <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="id_buku" class="form-control" id="id_buku" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nama" class="col-sm-3 col-form-label">Judul <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="judul" class="form-control" id="judul" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nama" class="col-sm-3 col-form-label">Penerbit <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="text" name="penerbit" class="form-control" id="penerbit" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="nama" class="col-sm-3 col-form-label">Tahun <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <input type="number" name="tahun" class="form-control" id="tahun" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Jenis Buku <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="jenis" name="jenis">
                                                                        <option value="0">- Pilih Jenis -</option>
                                                                        <?php while($jsn = pg_fetch_object($jenis)): 
                                                                            $id=$jsn->id_jenis;
                                                                            $nama=$jsn->nama_jenis;
                                                                        
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_jenis'])){if($id==$_SESSION['nama_jenis']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $nama;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Rak Buku <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="rak_buku" name="rak_buku">
                                                                        <option value="0">- Pilih Rak -</option>
                                                                        <?php while($rb = pg_fetch_object($rak_buku)): 
                                                                            $id=$rb->id_rak;
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['id_rak'])){if($id==$_SESSION['id_rak']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $id;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="no" class="col-sm-3 col-form-label">Penulis <span style='color:red'>*</span></label>
                                                                <div class="col-sm-7">
                                                                    <select class="form-control" id="penulis" name="penulis">
                                                                        <option value="0">- Pilih Penulis -</option>
                                                                        <?php while($pns = pg_fetch_object($penulis)): 
                                                                            $id=$pns->id_penulis;
                                                                            $nama=$pns->nama;
                                                                        
                                                                        ?>  
                                                                        <option value="<?php echo $id;?>"<?php if(!empty($_SESSION['nama_penulis'])){if($id==$_SESSION['nama_penulis']){?> selected="selected" <?php }}?>>
                                                                            <?php echo $nama;?>
                                                                        <?php endwhile; ?>
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="hobi" class="col-sm-3 col-form-label">Gambar</label>
                                                                <div class="col-sm-7">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="foto" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-body -->
                                                        <div class="card-footer">
                                                            <button type="submit" class="btn btn-info float-right" name="submit"  value="Submit">
                                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                                            </button>
                                                            <div class="text-left upgrade-btn">
                                                                <a href="buku.php" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                                    Kembali
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- /.card-footer -->
                                                    </form>
                                                </div>
                                                <!-- /.card -->
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("../includes/footer.php")?>
            <!-- /.footer -->
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php include("../includes/script.php")?>
    </body>
</html>