<?php
    session_start();
    include_once('../koneksi/koneksi.php');
    require('../oop/db_petugas.php');
    $obj = new Db_Petugas();
    if(isset($_POST['login'])&&!empty($_POST['login'])){
        if ((isset($_POST["username"]))&&(isset($_POST["password"]))){
            $username = $_POST["username"];
            $password = $_POST["password"];
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            $sql=$obj->login($username,$password);
            $login_check = pg_NumRows($sql);
            if($login_check > 0){ 
                echo '<script type="text/javascript">'; 
                echo 'alert("Yee Anda Berhasil Login");'; 
                echo 'window.location.href = "dashboard.php";';
                echo '</script>';    
            }else{
                echo '<script type="text/javascript">'; 
                echo 'alert("Anda Gagal Login");'; 
                echo 'window.location.href = "index.php";';
                echo '</script>';  
            }
        }
    }
?>
<head>
	<?php include("../includes/head.php")?>
</head>
<body>
    <div id="login">
        <div class="container">
            <h1 class="text-center text-white pt-5"> The Reading Room</h1>
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" method="post">
                            <br>
                            <h3 class="text-center text-info">LOGIN</h3>
                            
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="username" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <div class="col-4">
                                <button type="submit" id="ll" name="login" value="Login" class="btn center-block text-center">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>