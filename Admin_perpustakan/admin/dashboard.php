<?php 
session_start();
include_once('../koneksi/koneksi.php');
require('../oop/db_buku.php');
require('../oop/db_anggota.php');
require('../oop/db_penulis.php');
require('../oop/db_petugas.php');
$obj = new Db_Buku();
$obj2 = new Db_Anggota();
$obj3 = new Db_Penulis();
$obj4 = new Db_Petugas();
$buku = $obj->getBuku2();
$anggota = $obj2->getAnggota2();
$penulis = $obj3->getPenulis2();
$petugas = $obj4->getPetugas2();
?>
<!DOCTYPE html>
<html lang="en">
  <?php include("../includes/head.php")?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

      <!-- Navbar -->
      <?php include("../includes/navbar.php")?>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <?php include("../includes/sidebar.php")?>
      <!-- /.sidebar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Dashboard</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3><?= $buku?></h3>
                    <p>Buku</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-ios-book"></i>
                  </div>
                  <a href="buku.php" class="small-box-footer">
                    More info 
                    <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3><?= $anggota?></h3>
                    <p>Anggota</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-stalker "></i>
                  </div>
                  <a href="anggota.php" class="small-box-footer">
                    More info 
                    <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                  <h3><?= $penulis?></h3>
                    <p>Penulis</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-ios-compose"></i>
                  </div>
                  <a href="penulis.php" class="small-box-footer">More info 
                    <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3><?= $petugas?></h3>
                    <p>Petugas</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-briefcase"></i>
                  </div>
                  <a href="petugas.php" class="small-box-footer">
                    More info 
                    <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
              <!-- Column -->
              <div class="col-lg-12">
                <h3>Profil</h3>
              </div>
              <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body profile-card">
                        <center class="m-t-30"> 
                            <img src="../dist/img/dd2.jpg" class="rounded-circle" width="240" />
                        </center>
                    </div>
                </div>
              </div>
              <!-- Column -->
              <!-- Column -->
              <div class="col-lg-8 col-xlg-9 col-md-7">
                  <div class="card">
                      <div class="card-body">
                          <form class="form-horizontal form-material">
                              <div class="form-group">
                                  <label class="col-md-12 mb-0">Nama</label>
                                  <div class="col-md-12">
                                      <input type="text" placeholder="Ermadani Dyah R" class="form-control pl-0 form-control-line">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label for="example-email" class="col-md-12">NIM</label>
                                  <div class="col-md-12">
                                      <input type="text" placeholder="193140914111016" class="form-control pl-0 form-control-line">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-12 mb-0">Email</label>
                                  <div class="col-md-12">
                                      <input type="email" placeholder="ermadani.dyah09@gmail.com" class="form-control pl-0 form-control-line" name="example-email" id="example-email">
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
            </div>
            <!-- /.row (main row) -->
          </div>
          <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <!-- footer -->
      <?php include("../includes/footer.php")?>
      <!-- /.footer -->
      <!-- Control Sidebar -->
      <aside class="../control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <?php include("../includes/script.php")?>
  </body>
</html>