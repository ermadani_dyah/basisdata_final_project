<?php 
class Db_mata_kuliah{
    private $table_name = 'mata_kuliah';
    function createMataKuliah(){
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(kode_mata_kuliah,nama_mata_kuliah,kode_dosen) "."VALUES('".$this->cleanData($_POST['kode_mata_kuliah'])."','".$this->cleanData($_POST['nama_mata_kuliah'])."','".$this->cleanData($_POST['kode_dosen'])."')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchMataKuliah($kata){
        $sql="SELECT kode_mata_kuliah, nama_mata_kuliah, dosen.kode_dosen FROM mata_kuliah INNER JOIN dosen ON mata_kuliah.kode_dosen=dosen.kode_dosen where kode_mata_kuliah like '%".$kata."%' OR nama_mata_kuliah like '%".$kata."%'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getMataKuliah(){             
        $sql ="SELECT kode_mata_kuliah, nama_mata_kuliah, dosen.kode_dosen FROM mata_kuliah INNER JOIN dosen ON mata_kuliah.kode_dosen=dosen.kode_dosen ORDER BY kode_mata_kuliah ASC";
        return pg_query($sql);
    } 
    function getMataKuliahById(){    
        $sql="SELECT kode_mata_kuliah, nama_mata_kuliah, dosen.kode_dosen FROM mata_kuliah INNER JOIN dosen ON mata_kuliah.kode_dosen=dosen.kode_dosen where kode_mata_kuliah='".$this->cleanData($_POST['kode_dosen'])."'";
        return pg_query($sql);
    } 
    function deleteMataKuliah(){    
  
         $sql ="delete from public." . $this->table_name . "  where kode_mata_kuliah='".$this->cleanData($_POST['kode_mata_kuliah'])."'";
        return pg_query($sql);
    } 
    function updateMataKuliah($data=array()){       
     
        $sql = "update public.mata_kuliah set kode_mata_kuliah='".$this->cleanData($_POST['kode_mata_kuliah'])."',nama_mata_kuliah='".$this->cleanData($_POST['nama_mata_kuliah'])."', kode_dosen='".$this->cleanData($_POST['kode_dosen'])."' where kode_mata_kuliah = '".$this->cleanData($_POST['kode_mata_kuliah'])."' ";
        return pg_affected_rows(pg_query($sql));        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
