<?php
class Db_JenisBuku{
    private $table_name = 'jenis';
    function createJenisBuku(){
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(id_jenis,nama_jenis) "."VALUES('".$this->cleanData($_POST['id_jenis'])."','".$this->cleanData($_POST['nama_jenis'])."')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchJenisBuku($kata){
        $sql="select * from jenis where nama_jenis like '%".$kata."%' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getJenisBuku(){             
        $sql ="select *from public." . $this->table_name . " ORDER BY id_jenis ASC";
        return pg_query($sql);
    } 
    function getJenisBukuById(){    
  
        $sql ="select *from public." . $this->table_name . "  where id_jenis='".$this->cleanData($_POST['id_jenis'])."'";
        return pg_query($sql);
    } 
    function deleteJenisBuku(){    
  
         $sql ="delete from public." . $this->table_name . "  where id_jenis='".$this->cleanData($_POST['id_jenis'])."'";
        return pg_query($sql);
    } 
    function updateJenisBuku($data=array()){       
     
        $sql = "update public.jenis set id_jenis='".$this->cleanData($_POST['id_jenis'])."',nama_jenis='".$this->cleanData($_POST['nama_jenis'])."' where id_jenis = '".$this->cleanData($_POST['id_jenis'])."' ";
        return pg_affected_rows(pg_query($sql));        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
