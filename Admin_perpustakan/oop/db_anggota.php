<?php 
class Db_Anggota{
    private $table_name = 'anggota';
    function createAnggota(){
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(id_anggota,nama,jenis_kelamin,tanggal_lahir,alamat,no_telfon,email,username,password) "."VALUES('".$this->cleanData($_POST['id_anggota'])."','".$this->cleanData($_POST['nama_anggota'])."','".$this->cleanData($_POST['jenis_kelamin'])."','".$this->cleanData($_POST['tanggal_lahir'])."','".$this->cleanData($_POST['alamat_anggota'])."','".$this->cleanData($_POST['no_anggota'])."','".$this->cleanData($_POST['email_anggota'])."','".$this->cleanData($_POST['username'])."','".$this->cleanData($_POST['password'])."')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchAnggota($kata){
        $sql="select * from anggota where id_anggota like '%".$kata."%' OR nama like '%".$kata."%'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getAnggotaBaru($nama){
        $sql="select * from anggota where id_anggota='".$nama."'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getAnggotaBaruru($nama){
        $sql="select * from anggota where nama='".$nama."'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function login($username,$password){
        $sql ="select * from anggota where username = '".$username."' and password ='".$password."'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getAnggota(){             
        $sql ="select *from public." . $this->table_name . " ORDER BY id_anggota ASC";
        return pg_query($sql);
    } 
    function getAnggota2(){             
        $sql ="SELECT * FROM anggota ";
        $result = pg_query($sql);
        return pg_num_rows($result);;
    }
    function getAnggotaById(){    
  
        $sql ="select *from public." . $this->table_name . "  where id_anggota='".$this->cleanData($_POST['id_anggota'])."'";
        return pg_query($sql);
    } 
    function deleteAnggota(){    
  
         $sql ="delete from public." . $this->table_name . "  where id_anggota='".$this->cleanData($_POST['id_anggota'])."'";
        return pg_query($sql);
    } 
    function updateAnggota($data=array()){       
     
        $sql = "update public.anggota set id_anggota='".$this->cleanData($_POST['id_anggota'])."',nama='".$this->cleanData($_POST['nama_anggota'])."', no_telfon='".$this->cleanData($_POST['no_anggota'])."',email='".$this->cleanData($_POST['email_anggota'])."', alamat='".$this->cleanData($_POST['alamat_anggota'])."', jenis_kelamin='".$this->cleanData($_POST['jenis_kelamin'])."', tanggal_lahir='".$this->cleanData($_POST['tanggal_lahir'])."', username='".$this->cleanData($_POST['username'])."',password='".$this->cleanData($_POST['password'])."' where id_anggota = '".$this->cleanData($_POST['id_anggota'])."' ";
        return pg_affected_rows(pg_query($sql));        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
