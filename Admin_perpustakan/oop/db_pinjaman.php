<?php
class Db_Pinjaman{
    private $table_name = 'pinjaman';
    function createPinjaman(){
        $pinjam=$this->cleanData($_POST['tanggal_pinjaman']);
        $kembali=strtotime("+7 day", strtotime($pinjam));
        $kembali = date('Y-m-d', $kembali);
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(id_pinjaman,id_anggota,id_buku,id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman) "."VALUES('".$this->cleanData($_POST['id_pinjaman'])."','".$this->cleanData($_POST['anggota'])."','".$this->cleanData($_POST['buku'])."','".$this->cleanData($_POST['petugas'])."','$pinjam','$kembali','Dipinjam')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchPinjaman($kata){
        $sql="SELECT id_pinjaman,anggota.nama,buku.judul,buku.gambar,petugas.nama_petugas,petugas.id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman FROM pinjaman INNER JOIN anggota ON pinjaman.id_anggota=anggota.id_anggota INNER JOIN buku ON pinjaman.id_buku=buku.id_buku INNER JOIN petugas ON pinjaman.id_petugas=petugas.id_petugas where id_pinjaman like '%".$kata."%' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getPinjamKode($id_pinjaman){
        $sql="SELECT id_pinjaman,anggota.nama,buku.judul,buku.gambar,petugas.nama_petugas,petugas.id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman FROM pinjaman INNER JOIN anggota ON pinjaman.id_anggota=anggota.id_anggota INNER JOIN buku ON pinjaman.id_buku=buku.id_buku INNER JOIN petugas ON pinjaman.id_petugas=petugas.id_petugas where id_pinjaman='".$id_pinjaman."' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getPinjamKode2($id_anggota){
        $sql="SELECT id_pinjaman,anggota.nama,buku.judul,buku.gambar,petugas.nama_petugas,petugas.id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman FROM pinjaman INNER JOIN anggota ON pinjaman.id_anggota=anggota.id_anggota INNER JOIN buku ON pinjaman.id_buku=buku.id_buku INNER JOIN petugas ON pinjaman.id_petugas=petugas.id_petugas where pinjaman.id_anggota='".$id_anggota."' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getPinjaman(){             
        $sql ="SELECT id_pinjaman,anggota.nama,buku.judul,buku.gambar,petugas.nama_petugas,petugas.id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman FROM pinjaman INNER JOIN anggota ON pinjaman.id_anggota=anggota.id_anggota INNER JOIN buku ON pinjaman.id_buku=buku.id_buku INNER JOIN petugas ON pinjaman.id_petugas=petugas.id_petugas  ORDER BY id_pinjaman ASC";
        return pg_query($sql);
    }
    function getPinjamanById(){    
        $sql="SELECT id_pinjaman,anggota.nama,buku.judul,buku.gambar,petugas.nama_petugas,petugas.id_petugas,tanggal_pinjaman,tanggal_kembali,status_pinjaman FROM pinjaman INNER JOIN anggota ON pinjaman.id_anggota=anggota.id_anggota INNER JOIN buku ON pinjaman.id_buku=buku.id_buku INNER JOIN petugas ON pinjaman.id_petugas=petugas.id_petugas where id_pinjaman='".$this->cleanData($_POST['id_pinjaman'])."'";
        return pg_query($sql);
    } 
    function getUpdateStatus1($id_pinjaman){    
        $sql = "update pinjaman set status_pinjaman='Dipinjam' where id_pinjaman='$id_pinjaman'";
        return pg_query($sql);
    } 
    function getUpdateStatus2($id_pinjaman){    
        $sql = "update pinjaman set status_pinjaman='Terlambat' where id_pinjaman='$id_pinjaman'";
        return pg_query($sql);
    } 
    function deletePinjaman(){    
         $sql ="delete from public." . $this->table_name . "  where id_pinjaman='".$this->cleanData($_POST['id_pinjaman'])."'";
        return pg_query($sql);
    } 
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
