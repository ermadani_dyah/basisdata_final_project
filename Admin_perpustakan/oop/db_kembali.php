<?php
class Db_Kembali{
    private $table_name = 'kembali';
    function createKembali(){
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(id_kembali,id_pinjaman,id_petugas,tgl_kembali,status_kembali) "."VALUES('".$this->cleanData($_POST['id_kembali'])."','".$this->cleanData($_POST['pinjaman'])."','".$this->cleanData($_POST['petugas'])."','".$this->cleanData($_POST['tanggal_kembali'])."','Belum Dibayar')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchKembali($kata){
        $sql="SELECT id_kembali,pinjaman.id_pinjaman,pinjaman.tanggal_pinjaman,pinjaman.tanggal_kembali,petugas.nama_petugas,tgl_kembali,status_kembali FROM kembali INNER JOIN pinjaman ON kembali.id_pinjaman=pinjaman.id_pinjaman INNER JOIN petugas ON kembali.id_petugas=petugas.id_petugas where id_kembali like '%".$kata."%' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getKembali(){             
        $sql ="SELECT id_kembali,pinjaman.id_pinjaman,pinjaman.tanggal_pinjaman,pinjaman.tanggal_kembali,petugas.nama_petugas,tgl_kembali,status_kembali FROM kembali INNER JOIN pinjaman ON kembali.id_pinjaman=pinjaman.id_pinjaman INNER JOIN petugas ON kembali.id_petugas=petugas.id_petugas  ORDER BY id_kembali ASC";
        return pg_query($sql);
    }
    function getKembaliById(){    
        $sql="SELECT id_kembali,pinjaman.id_pinjaman,pinjaman.tanggal_pinjaman,pinjaman.tanggal_kembali,petugas.nama_petugas,tgl_kembali,status_kembali FROM kembali INNER JOIN pinjaman ON kembali.id_pinjaman=pinjaman.id_pinjaman INNER JOIN petugas ON kembali.id_petugas=petugas.id_petugas where id_kembali='".$this->cleanData($_POST['id_kembali'])."'";
        return pg_query($sql);
    } 
    function getUpdateStatus1($id_kembali,$denda){    
        $sql = "update kembali set denda='$denda' where id_kembali='$id_kembali'";
        return pg_query($sql);
    }
    function deleteKembali(){    
         $sql ="delete from public." . $this->table_name . "  where id_kembali='".$this->cleanData($_POST['id_kembali'])."'";
        return pg_query($sql);
    } 
    function updateKembali($data=array()){       
        $sql = "update public.kembali set status_kembali='Dibayar' where id_kembali = '".$this->cleanData($_POST['id_kembali'])."' ";
        return pg_affected_rows(pg_query($sql));        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
