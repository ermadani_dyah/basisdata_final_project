<?php
class Db_Buku{
    private $table_name = 'buku';
    function createBuku($lokasi_file){
        $id=$this->cleanData($_POST['id_buku']);
        $judul=$this->cleanData($_POST['judul']);
		$direktori = '../dist/img/'.$id.'.jpg';
		if(move_uploaded_file($lokasi_file,$direktori)){
            $nama_file = $id.'.jpg';
			$sql = "INSERT INTO PUBLIC.".$this->table_name."(id_buku,judul,penerbit,tahun_terbit,id_jenis_buku,id_rak_buku,id_penulis,gambar) "."VALUES('".$this->cleanData($_POST['id_buku'])."','".$this->cleanData($_POST['judul'])."','".$this->cleanData($_POST['penerbit'])."','".$this->cleanData($_POST['tahun'])."','".$this->cleanData($_POST['jenis'])."','".$this->cleanData($_POST['rak_buku'])."','".$this->cleanData($_POST['penulis'])."','".$nama_file."')";
            return pg_affected_rows(pg_query($sql));
		}else{
			$sql = "INSERT INTO PUBLIC.".$this->table_name."(id_buku,judul,penerbit,tahun_terbit,id_jenis_buku,id_rak_buku,id_penulis) "."VALUES('".$this->cleanData($_POST['id_buku'])."','".$this->cleanData($_POST['judul'])."','".$this->cleanData($_POST['penerbit'])."','".$this->cleanData($_POST['tahun'])."','".$this->cleanData($_POST['jenis'])."','".$this->cleanData($_POST['rak_buku'])."','".$this->cleanData($_POST['penulis'])."')";
            return pg_affected_rows(pg_query($sql));
		}
    }
    function searchBuku($kata){
        $sql="SELECT id_buku,judul,penerbit,tahun_terbit,jenis.nama_jenis,rak_buku.id_rak,penulis.nama,gambar FROM buku INNER JOIN jenis ON buku.id_jenis_buku=jenis.id_jenis INNER JOIN rak_buku ON buku.id_rak_buku=rak_buku.id_rak INNER JOIN penulis ON buku.id_penulis=penulis.id_penulis where judul like '%".$kata."%' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getBukuKode($id_buku){
        $sql="SELECT id_buku,judul,penerbit,tahun_terbit,jenis.nama_jenis,rak_buku.id_rak,penulis.nama,gambar FROM buku INNER JOIN jenis ON buku.id_jenis_buku=jenis.id_jenis INNER JOIN rak_buku ON buku.id_rak_buku=rak_buku.id_rak INNER JOIN penulis ON buku.id_penulis=penulis.id_penulis where id_buku = '".$id_buku."' ";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getBuku(){             
        $sql ="SELECT id_buku,judul,penerbit,tahun_terbit,jenis.nama_jenis,rak_buku.id_rak,penulis.nama,gambar FROM buku INNER JOIN jenis ON buku.id_jenis_buku=jenis.id_jenis INNER JOIN rak_buku ON buku.id_rak_buku=rak_buku.id_rak INNER JOIN penulis ON buku.id_penulis=penulis.id_penulis ORDER BY id_buku ASC";
        return pg_query($sql);
    } 
    function getBuku2(){             
        $sql ="SELECT * FROM buku ";
        $result = pg_query($sql);
        return pg_num_rows($result);;
    }
    function getBukuById(){    
        $sql="SELECT id_buku,judul,penerbit,tahun_terbit,jenis.nama_jenis,rak_buku.id_rak,penulis.nama,gambar FROM buku INNER JOIN jenis ON buku.id_jenis_buku=jenis.id_jenis INNER JOIN rak_buku ON buku.id_rak_buku=rak_buku.id_rak INNER JOIN penulis ON buku.id_penulis=penulis.id_penulis where id_buku='".$this->cleanData($_POST['id_buku'])."'";
        return pg_query($sql);
    } 
    function deleteBuku(){    
  
         $sql ="delete from public." . $this->table_name . "  where id_buku='".$this->cleanData($_POST['id_buku'])."'";
        return pg_query($sql);
    } 
    function updateBuku($data=array()){       
        $id=$this->cleanData($_POST['id_buku']);
        $judul=$this->cleanData($_POST['judul']);
        $lokasi_file = $_FILES['foto']['tmp_name'];
		$direktori = '../dist/img/'.$id.'.jpg';
        if(move_uploaded_file($lokasi_file,$direktori)){
            $nama_file = $id.'.jpg';
			$sql = "update public.buku set id_buku='".$this->cleanData($_POST['id_buku'])."',judul='".$this->cleanData($_POST['judul'])."', penerbit='".$this->cleanData($_POST['penerbit'])."', tahun_terbit='".$this->cleanData($_POST['tahun'])."', id_jenis_buku='".$this->cleanData($_POST['jenis'])."', id_rak_buku='".$this->cleanData($_POST['rak_buku'])."',id_penulis='".$this->cleanData($_POST['penulis'])."',gambar='".$nama_file."' where id_buku = '".$this->cleanData($_POST['id_buku'])."' ";
            return pg_affected_rows(pg_query($sql));
		}else{
			$sql = "update public.buku set id_buku='".$this->cleanData($_POST['id_buku'])."',judul='".$this->cleanData($_POST['judul'])."', penerbit='".$this->cleanData($_POST['penerbit'])."', tahun_terbit='".$this->cleanData($_POST['tahun'])."', id_jenis_buku='".$this->cleanData($_POST['jenis'])."', id_rak_buku='".$this->cleanData($_POST['rak_buku'])."',id_penulis='".$this->cleanData($_POST['penulis'])."' where id_buku = '".$this->cleanData($_POST['id_buku'])."' ";
            return pg_affected_rows(pg_query($sql));
		}        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
