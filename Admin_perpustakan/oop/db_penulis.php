<?php
class Db_Penulis{
    private $table_name = 'penulis';
    function createPenulis(){
        $sql = "INSERT INTO PUBLIC.".$this->table_name."(id_penulis,nama,no_telfon,email,alamat) "."VALUES('".$this->cleanData($_POST['id_penulis'])."','".$this->cleanData($_POST['nama_penulis'])."','".$this->cleanData($_POST['no_penulis'])."','".$this->cleanData($_POST['email_penulis'])."','".$this->cleanData($_POST['alamat_penulis'])."')";
        return pg_affected_rows(pg_query($sql));
    }
    function searchPenulis($kata){
        $sql="select * from penulis where id_penulis like '%".$kata."%' OR nama like '%".$kata."%'";
        $result_set = pg_query($sql);
        return $result_set;
    }
    function getPenulis(){             
        $sql ="select *from public." . $this->table_name . " ORDER BY id_penulis ASC";
        return pg_query($sql);
    } 
    function getPenulis2(){             
        $sql ="SELECT * FROM penulis ";
        $result = pg_query($sql);
        return pg_num_rows($result);;
    }
    function getPenulisById(){    
  
        $sql ="select *from public." . $this->table_name . "  where id_penulis='".$this->cleanData($_POST['id_penulis'])."'";
        return pg_query($sql);
    } 
    function deletePenulis(){    
  
         $sql ="delete from public." . $this->table_name . "  where id_penulis='".$this->cleanData($_POST['id_penulis'])."'";
        return pg_query($sql);
    } 
    function updatePenulis($data=array()){       
     
        $sql = "update public.penulis set id_penulis='".$this->cleanData($_POST['id_penulis'])."',nama='".$this->cleanData($_POST['nama_penulis'])."', no_telfon='".$this->cleanData($_POST['no_penulis'])."',email='".$this->cleanData($_POST['email_penulis'])."', alamat='".$this->cleanData($_POST['alamat_penulis'])."' where id_penulis = '".$this->cleanData($_POST['id_penulis'])."' ";
        return pg_affected_rows(pg_query($sql));        
    }
    function cleanData($val){
         return pg_escape_string($val);
    }
}
?> 
