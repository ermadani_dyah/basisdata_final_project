<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">Terimakasih</h2>
                    <p class="col-12 text-center">Terimakasih Sudah Melakukan Transaksi</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="mb-5"> 
                        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                            <tbody style="text-align:center;">  
                                <tr>
                                    <td colspan="2">
                                        <i class="fas fa-check"></i>
                                        <strong>Status Berhasil</strong>
                                        <br>
                                        Terimakasih Sudah melakukan transaksi dan penyewaan samapai jumpa lagi
                                        <br>
                                        <a href="index.php" class="tm-btn tm-btn-default tm-right">Kembali</a>
                                    </td>
                                </tr>               
                            </tbody>
                        </table> 
                    </div>
                </div>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>