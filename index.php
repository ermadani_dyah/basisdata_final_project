<?php 
	session_start();
	include_once('Admin_perpustakan/koneksi/koneksi.php');
	require('Admin_perpustakan/oop/db_buku.php');
	$obj = new Db_Buku();
	$buku= $obj->getBuku();
	$sn=1;
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar.php")?>
			<main>
				<header class="row tm-welcome-section">
					<h2 class="col-12 text-center tm-section-title">Baca Bukumu</h2>
					<p class="col-12 text-center">Membaca membentuk impianku dan lebih banyak membaca membantu impianku menjadi kenyataan</p>
				</header>
				<br>
				<!-- Gallery -->
				<div class="row tm-gallery">
					<!-- gallery page 1 -->
					<div id="tm-gallery-page-canon" class="tm-gallery-page">
						<?php while($bk = pg_fetch_object($buku)): ?>
							
							<article class="col-lg-3 col-md-4 col-sm-6 col-12 tm-gallery-item">
								<figure>
									<img src="Admin_perpustakan/dist/img/<?php echo $bk->gambar;?>" alt="Image" class="img-fluid tm-gallery-img" style="width: 200px;height: 250px;"/>
									<figcaption>
										<h4 class="tm-gallery-title"><?=$bk->judul?></h4>
										<p class="tm-gallery-description">Penulis: <?=$bk->nama?></p>
										<p class="tm-gallery-description">Tahun: <?=$bk->tahun_terbit?></p>
										<p class="tm-gallery-price">Kategori: <?=$bk->nama_jenis?></p>
										<a href="login.php?data=<?php echo $bk->id_buku;?>" class="tm-gallery-price tm-btn tm-btn-default tm-right">Pinjam</a>
									</figcaption>
								</figure>
							</article>
						<?php endwhile; ?>
					</div>
					<!-- gallery page 1 -->
				</div>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>