<?php 
session_start();
include_once('Admin_perpustakan/koneksi/koneksi.php');
require('Admin_perpustakan/oop/db_anggota.php');
require('Admin_perpustakan/oop/db_buku.php');
require('Admin_perpustakan/oop/db_petugas.php');
require('Admin_perpustakan/oop/db_pinjaman.php');
require('Admin_perpustakan/oop/db_kembali.php');
$obj = new Db_Anggota();
$obj1 = new Db_Kembali();
$obj2 = new Db_Petugas();
$obj3 = new Db_Pinjaman();
$id_pelanggan = $_SESSION['id_pelanggan'];
$sql=$obj->getAnggotaBaru($id_pelanggan);
$rows = pg_NumRows($sql);
for ($j=0; $j<$rows; $j++){
    $id_anggota= pg_result($sql, $j, "id_anggota");
    $nama= pg_result($sql, $j, "nama");
    $data=$obj3->getPinjamKode2($id_anggota);
    $check = pg_NumRows($data);
    for ($j=0; $j<$check; $j++){
        $id_pinjaman= pg_result($data, $j, "id_pinjaman");
        $petugas= pg_result($data, $j, "nama");
        $id_petugas= pg_result($data, $j, "id_petugas");
    }
}
if(isset($_POST['submit']) and !empty($_POST['submit'])){
$ret_val = $obj1->createKembali();
if($ret_val==1){
    echo '<script type="text/javascript">'; 
    echo 'alert("Record Saved Successfully");'; 
    echo 'window.location.href = "status.php";';
    echo '</script>';
}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">Pengembalian Buku</h2>
                    <p class="col-12 text-center">Silahkan Lakukan Pengembalian Anda</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form h-100">
                                <h3>Get Started</h3>
                                <form class="mb-5" enctype="multipart/form-data" method="post" id="contactForm" name="contactForm" >
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">ID Pengembalian *</label>
                                            <input type="text" class="form-control" name="id_kembali" id="id_kembali"  value="">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">ID Transaksi *</label>
                                            <input type="text" class="form-control" name="pinjaman" id="pinjaman"  value="<?=$id_pinjaman?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">ID Pelanggan *</label>
                                            <input type="text" class="form-control"  id="nama_pelanggan"  value="<?=$id_anggota?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">ID Petugas *</label>
                                            <input type="text" class="form-control" name="petugas" id="petugas"  value="<?=$id_petugas?>">
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Tanggal Pengembalian Kembali *</label>
                                            <input type="date" class="form-control" name="tanggal_kembali" id="tanggal_kembali" >
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="" class="col-form-label">Pembayaran Denda *</label>
                                            <input type="file" class="form-control"  id="customFile">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <button type="submit" class="tm-btn tm-btn-default tm-right" name="submit"  value="Submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>