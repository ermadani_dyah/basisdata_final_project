<?php 
session_start();
include_once('Admin_perpustakan/koneksi/koneksi.php');
require('Admin_perpustakan/oop/db_anggota.php');
require('Admin_perpustakan/oop/db_buku.php');
require('Admin_perpustakan/oop/db_petugas.php');
require('Admin_perpustakan/oop/db_pinjaman.php');
$obj = new Db_Anggota();
$obj1 = new Db_Buku();
$obj2 = new Db_Petugas();
$obj3 = new Db_Pinjaman();
if(isset($_GET['data'])){
    $id_transaksi = $_GET['data'];
    $_SESSION['id_transaksi']=$id_transaksi;

$data=$obj3->getPinjamKode($id_transaksi);
$check = pg_NumRows($data);
for ($j=0; $j<$check; $j++){
    $id_pinjaman= pg_result($data, $j, "id_pinjaman");
    $anggota= pg_result($data, $j, "nama");
    $buku= pg_result($data, $j, "judul");
    $gambar= pg_result($data, $j, "gambar");
    $petugas= pg_result($data, $j, "nama");
    $tanggal_pinjaman= pg_result($data, $j, "tanggal_pinjaman");
    $tanggal_kembali= pg_result($data, $j, "tanggal_kembali");
    $status_pinjaman= pg_result($data, $j, "status_pinjaman");
    $masaberlaku = strtotime($tanggal_kembali) - strtotime("$tanggal_pinjaman") ;
    $waktu=$masaberlaku/(24*60*60);
    $denda=$waktu*5000;
    $sql=$obj->getAnggotaBaruru($anggota);
    $rows = pg_NumRows($sql);
    for ($j=0; $j<$rows; $j++){
        $id_anggota= pg_result($sql, $j, "id_anggota");
        $nama= pg_result($sql, $j, "nama");
        $jenis= pg_result($sql, $j, "jenis_kelamin");
        $tanggal_lahir= pg_result($sql, $j, "tanggal_lahir");
        $alamat= pg_result($sql, $j, "alamat");
        $no= pg_result($sql, $j, "no_telfon");
        $email= pg_result($sql, $j, "email");
        $username= pg_result($sql, $j, "username");
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar_profil.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">Profil Anda ANDA</h2>
                    <p class="col-12 text-center">Informasi Transaksi Anda</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="row"> 
                        <article class="col-lg-6">
                            <figure class="tm-person">
                                <img src="img/about-01.jpg" alt="Image" class="img-fluid tm-person-img" />
                                <figcaption class="tm-person-description">
                                    <h4 class="tm-person-name"><?php echo $anggota?></h4>
                                    <p class="tm-person-title">Jenis Kelamin: <?php echo $jenis?></p>
                                    <p class="tm-person-about">Alamat: <?php echo $alamat?></p>
                                    <p class="tm-person-about"><i class="fas fa-envelope "></i> : <?php echo $email?></p>
                                    <p class="tm-person-about"><i class="fab fa-whatsapp"></i> : <?php echo $no?></p>
                                </figcaption>
                            </figure>
                        </article>
                        <article class="col-lg-6">
                            <figure class="tm-person">
                                <img src="Admin_perpustakan/dist/img/<?php echo $gambar;?>" alt="Image" class="img-fluid tm-person-img" style="width:250px" />
                                <figcaption class="tm-person-description">
                                    <h4 class="tm-person-name">Transaksi Anda</h4>
                                    <p class="tm-person-about">Buku: <?php echo $buku?></p>
                                    <p class="tm-person-about">Tanggal Pinjam: <?php echo $tanggal_pinjaman?></p>
                                    <p class="tm-person-about">Tanggal Kembali: <?php echo $tanggal_kembali?></p>
                                    <p class="tm-person-title">Status: <?php echo $status_pinjaman?></p>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                </div>
                <div class="tm-container-inner tm-persons">
                    <div class="mb-5"> 
                        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                            <tbody>  
                                <tr>
                                    <td colspan="2">
                                        <h4 class="tm-person-name">Denda Anda</h4>
                                    </td>
                                </tr>               
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Pinjam:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $tanggal_pinjaman?>
                                    </td>
                                </tr>  
                                <tr>
                                    <td width="20%">
                                        <strong>Tanggal Jatuh Tempo:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $tanggal_kembali?>
                                    </td>
                                </tr>                  
                                <tr>
                                    <td width="20%">
                                        <strong>Terlambat:   </strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $waktu?>
                                    </td>
                                </tr> 
                                <tr>
                                    <td width="20%">
                                        <strong>Denda:   <strong>
                                    </td>
                                    <td width="80%">
                                        <?php echo $denda?>
                                    </td>
                                </tr>
                            </tbody>
                        </table> 
                        <br>
                        <hr>
                        <!--Jika Maka Proses pengembalian-->
                            <h3>Terimakasih Sudah Mempercayai Kami</h3>
                            <p>
                                Semoga Hari Anda Menyenangkan dan Stay Healty
                            </p>
                            <p>Buku adalah teman yang berharga. Namun, sulit untuk menjelaskan hal itu kepada yang tak suka membaca</p>
                            <br>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <a href="index.php" class="tm-btn tm-btn-default tm-right">Kembali</a>
                                </div>
                            </div>
                    </div>
                </div>
<?php
    }else if(isset($_GET['pelanggan'])){
        $id_pelanggan = $_GET['pelanggan'];
        $_SESSION['id_pelanggan']=$id_pelanggan;
        $sql=$obj->getAnggotaBaru($id_pelanggan);
        $rows = pg_NumRows($sql);
        for ($j=0; $j<$rows; $j++){
            $id_anggota= pg_result($sql, $j, "id_anggota");
            $nama= pg_result($sql, $j, "nama");
            $jenis= pg_result($sql, $j, "jenis_kelamin");
            $tanggal_lahir= pg_result($sql, $j, "tanggal_lahir");
            $alamat= pg_result($sql, $j, "alamat");
            $no= pg_result($sql, $j, "no_telfon");
            $email= pg_result($sql, $j, "email");
            $username= pg_result($sql, $j, "username");
            $data=$obj3->getPinjamKode2($id_anggota);
            $check = pg_NumRows($data);
            for ($j=0; $j<$check; $j++){
                $id_pinjaman= pg_result($data, $j, "id_pinjaman");
                $anggota= pg_result($data, $j, "nama");
                $buku= pg_result($data, $j, "judul");
                $gambar= pg_result($data, $j, "gambar");
                $petugas= pg_result($data, $j, "nama");
                $tanggal_pinjaman= pg_result($data, $j, "tanggal_pinjaman");
                $tanggal_kembali= pg_result($data, $j, "tanggal_kembali");
                $status_pinjaman= pg_result($data, $j, "status_pinjaman");
                $masaberlaku = strtotime($tanggal_kembali) - strtotime("$tanggal_pinjaman") ;
                $waktu=$masaberlaku/(24*60*60);
                $denda=$waktu*5000;
            }
        }
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include("includes/head.php");?>
	</head>
	<body> 
		<div class="container">
		<!-- Top box -->
			<?php include("includes/navbar_profil.php")?>
			<main>
                <header class="row tm-welcome-section">
                    <h2 class="col-12 text-center tm-section-title">Profil Anda ANDA</h2>
                    <p class="col-12 text-center">Informasi Transaksi Anda</p>
                </header>
                <div class="tm-container-inner tm-persons">
                    <div class="row">
                        <article class="col-lg-6">
                            <figure class="tm-person">
                                <img src="img/about-01.jpg" alt="Image" class="img-fluid tm-person-img" />
                                <figcaption class="tm-person-description">
                                    <h4 class="tm-person-name"><?php echo $nama?></h4>
                                    <p class="tm-person-title">Jenis Kelamin: <?php echo $jenis?></p>
                                    <p class="tm-person-about">Alamat: <?php echo $alamat?></p>
                                    <p class="tm-person-about"><i class="fas fa-envelope "></i> : <?php echo $email?></p>
                                    <p class="tm-person-about"><i class="fab fa-whatsapp"></i> : <?php echo $nama?></p>
                                </figcaption>
                            </figure>
                        </article>
                        <?php
                            if(isset($id_pinjaman)){
                        ?>
                                <article class="col-lg-6">
                                    <figure class="tm-person">
                                        <img src="Admin_perpustakan/dist/img/<?php echo $gambar;?>" alt="Image" class="img-fluid tm-person-img" style="width:250px" />
                                        <figcaption class="tm-person-description">
                                            <h4 class="tm-person-name">Transaksi Anda</h4>
                                            <p class="tm-person-about">Buku: <?php echo $buku?></p>
                                            <p class="tm-person-about">Tanggal Pinjam: <?php echo $tanggal_pinjaman?></p>
                                            <p class="tm-person-about">Tanggal Kembali: <?php echo $tanggal_kembali?></p>
                                            <p class="tm-person-title">Status: <?php echo $status_pinjaman?></p>
                                        </figcaption>
                                    </figure>
                                </article>
                    </div>
                </div>
                                <div class="tm-container-inner tm-persons">
                                    <div class="mb-5"> 
                                        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                                            <tbody>  
                                                <tr>
                                                    <td colspan="2">
                                                        <h4 class="tm-person-name">Denda Anda</h4>
                                                    </td>
                                                </tr>               
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Tanggal Pinjam:   </strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $tanggal_pinjaman?>
                                                    </td>
                                                </tr>  
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Tanggal Jatuh Tempo:   </strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $tanggal_kembali?>
                                                    </td>
                                                </tr>                  
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Terlambat:   </strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $waktu?>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Denda:   <strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $denda?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                        <br>
                                        <hr>
                                        <?php 
                                            if($waktu==0){
                                        ?>
                                        <!--Jika Maka Proses pengembalian-->
                                            <h3>Terimakasih Sudah Mempercayai Kami</h3>
                                            <p>
                                                Silahkan anda melakukan pengembalian Dibutton bawah ini
                                            </p>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <a href="pengembalian.php" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                                                </div>
                                            </div>
                                        <?php
                                            }else if($waktu>=1){
                                        ?>
                                        <!--Jika Maka Proses pengembalian-->
                                            <h3>Silahkan Lakukan Pembayaran Denda Jika Anda Terlambat</h3>
                                            <p>
                                                Silahkan anda membayar tagihan anda dengan cara transfer via Bank BRI di nomor Rekening : <br>
                                                <strong>(0986-01-025805-53-8 a/n Ermadani Dyah)</strong> untuk menyelesaikan pembayaran. dan untuk uang pembayaran denda
                                            </p>
                                            <p>Status akan berubah dalam 1x24 jam</p>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <a href="pengembalian.php" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                                                </div>
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
<?php
                            }
}
?>
			</main>
			<?php include("includes/footer.php")?>
		</div>
		<?php include("includes/script.php")?>
	</body>
</html>