PGDMP         )    
            y            Perpustakaan    13.2    13.2     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16483    Perpustakaan    DATABASE     r   CREATE DATABASE "Perpustakaan" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';
    DROP DATABASE "Perpustakaan";
                postgres    false            �            1259    16508    anggota    TABLE     o  CREATE TABLE public.anggota (
    id_anggota character varying NOT NULL,
    nama character varying,
    jenis_kelamin character varying,
    tanggal_lahir character varying(30),
    alamat character varying,
    email character varying,
    no_telfon character varying,
    username character varying,
    password character varying,
    gambar character varying
);
    DROP TABLE public.anggota;
       public         heap    postgres    false            �            1259    16531    buku    TABLE     #  CREATE TABLE public.buku (
    id_buku character varying NOT NULL,
    judul character varying,
    penerbit character varying,
    tahun_terbit bigint,
    id_jenis_buku character varying,
    id_rak_buku character varying,
    id_penulis character varying,
    gambar character varying
);
    DROP TABLE public.buku;
       public         heap    postgres    false            �            1259    16523    jenis    TABLE     i   CREATE TABLE public.jenis (
    id_jenis character varying NOT NULL,
    nama_jenis character varying
);
    DROP TABLE public.jenis;
       public         heap    postgres    false            �            1259    16563    kembali    TABLE     �   CREATE TABLE public.kembali (
    id_kembali character varying NOT NULL,
    id_pinjaman character varying,
    id_petugas character varying,
    tgl_kembali date,
    denda bigint,
    status_kembali character varying
);
    DROP TABLE public.kembali;
       public         heap    postgres    false            �            1259    16500    penulis    TABLE     �   CREATE TABLE public.penulis (
    id_penulis character varying NOT NULL,
    nama character varying,
    no_telfon bigint,
    email character varying,
    alamat character varying
);
    DROP TABLE public.penulis;
       public         heap    postgres    false            �            1259    16492    petugas    TABLE     �   CREATE TABLE public.petugas (
    id_petugas character varying NOT NULL,
    nama_petugas character varying,
    no_telfon bigint,
    alamat character varying,
    username character varying,
    password character varying
);
    DROP TABLE public.petugas;
       public         heap    postgres    false            �            1259    16555    pinjaman    TABLE       CREATE TABLE public.pinjaman (
    id_pinjaman character varying NOT NULL,
    id_anggota character varying,
    id_buku character varying,
    id_petugas character varying,
    tanggal_pinjaman date,
    tanggal_kembali date,
    status_pinjaman character varying
);
    DROP TABLE public.pinjaman;
       public         heap    postgres    false            �            1259    16484    rak_buku    TABLE     �   CREATE TABLE public.rak_buku (
    id_rak character varying NOT NULL,
    kapasitas character varying,
    lokasi character varying
);
    DROP TABLE public.rak_buku;
       public         heap    postgres    false            �          0    16508    anggota 
   TABLE DATA           �   COPY public.anggota (id_anggota, nama, jenis_kelamin, tanggal_lahir, alamat, email, no_telfon, username, password, gambar) FROM stdin;
    public          postgres    false    203   $        �          0    16531    buku 
   TABLE DATA           v   COPY public.buku (id_buku, judul, penerbit, tahun_terbit, id_jenis_buku, id_rak_buku, id_penulis, gambar) FROM stdin;
    public          postgres    false    205   �        �          0    16523    jenis 
   TABLE DATA           5   COPY public.jenis (id_jenis, nama_jenis) FROM stdin;
    public          postgres    false    204   �!       �          0    16563    kembali 
   TABLE DATA           j   COPY public.kembali (id_kembali, id_pinjaman, id_petugas, tgl_kembali, denda, status_kembali) FROM stdin;
    public          postgres    false    207   D"       �          0    16500    penulis 
   TABLE DATA           M   COPY public.penulis (id_penulis, nama, no_telfon, email, alamat) FROM stdin;
    public          postgres    false    202   �"       �          0    16492    petugas 
   TABLE DATA           b   COPY public.petugas (id_petugas, nama_petugas, no_telfon, alamat, username, password) FROM stdin;
    public          postgres    false    201   #       �          0    16555    pinjaman 
   TABLE DATA           �   COPY public.pinjaman (id_pinjaman, id_anggota, id_buku, id_petugas, tanggal_pinjaman, tanggal_kembali, status_pinjaman) FROM stdin;
    public          postgres    false    206   �#       �          0    16484    rak_buku 
   TABLE DATA           =   COPY public.rak_buku (id_rak, kapasitas, lokasi) FROM stdin;
    public          postgres    false    200   )$       K           2606    16515    anggota anggota_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (id_anggota);
 >   ALTER TABLE ONLY public.anggota DROP CONSTRAINT anggota_pkey;
       public            postgres    false    203            O           2606    16538    buku buku_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.buku
    ADD CONSTRAINT buku_pkey PRIMARY KEY (id_buku);
 8   ALTER TABLE ONLY public.buku DROP CONSTRAINT buku_pkey;
       public            postgres    false    205            M           2606    16530    jenis jenis_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.jenis
    ADD CONSTRAINT jenis_pkey PRIMARY KEY (id_jenis);
 :   ALTER TABLE ONLY public.jenis DROP CONSTRAINT jenis_pkey;
       public            postgres    false    204            S           2606    16570    kembali kembali_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.kembali
    ADD CONSTRAINT kembali_pkey PRIMARY KEY (id_kembali);
 >   ALTER TABLE ONLY public.kembali DROP CONSTRAINT kembali_pkey;
       public            postgres    false    207            I           2606    16507    penulis penulis_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.penulis
    ADD CONSTRAINT penulis_pkey PRIMARY KEY (id_penulis);
 >   ALTER TABLE ONLY public.penulis DROP CONSTRAINT penulis_pkey;
       public            postgres    false    202            G           2606    16499    petugas petugas_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.petugas
    ADD CONSTRAINT petugas_pkey PRIMARY KEY (id_petugas);
 >   ALTER TABLE ONLY public.petugas DROP CONSTRAINT petugas_pkey;
       public            postgres    false    201            Q           2606    16562    pinjaman pinjaman_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.pinjaman
    ADD CONSTRAINT pinjaman_pkey PRIMARY KEY (id_pinjaman);
 @   ALTER TABLE ONLY public.pinjaman DROP CONSTRAINT pinjaman_pkey;
       public            postgres    false    206            E           2606    16491    rak_buku rak_buku_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.rak_buku
    ADD CONSTRAINT rak_buku_pkey PRIMARY KEY (id_rak);
 @   ALTER TABLE ONLY public.rak_buku DROP CONSTRAINT rak_buku_pkey;
       public            postgres    false    200            �   �   x�}���PE��+�H�v���)F]��F|�Cd�ߋ�`�6]����%,�Ō������ �2�`��.l]�A3ǧ��(5��ppDQ��Ȇ �%da��ƈL땘tXs�撓m=!,I�M�s?)�ߺ'߱Og�QjG\8��/Ťl�]+�_����טB�+#&����z��L�T�/Q߱7���<��M�m      �   �   x�}��N�0���S�	�i���hB��7���.�C(���f7e�n���k[ 
(߫�����.�t
��^��9�p�owQ1��&;�S�Oi~N'!-�rv���u�ZM��2�l��ɳgeZ�Ū>A1Aq�� נ�w�,{�j؆|Ϟ�6��.�:��4kɨ�
��4$�;uxyk��DD~YzA�$�%a�M�)�Y�i,;7���/��c�����v�<���=���x��R      �   9   x��200�tK�+I,�,��200���M�KNqL8��sSS2���b@����� �M7      �   N   x�p500��� ����P��L����������%3)�2��+ ��������T�Ȃ�� ��)5�4W�>F��� �u�      �   \   x�p500�t-�MLI���0455511274�	�T&fX:��&f��%��rz��&%�s 5�5*� �)��O��a�ᛘ2"F��� �Y%�      �   �   x�000��IMU�J���0455511274��/���IM��ɤ�&�$�eXr���$�V��c�U�,ʢ�4�t��\*3�Pt�&�$"i�O*A3���;3W�%b8��)�x ��1F���  �FF      �   x   x��α
�@���_.��K%��)mN�P����ā�ze�a�y0�g�Et����ϕ�ڸ׼�ےWJ���(����и������<��ר0L�Q;�{O�'�c>ix���7�      �   1   x�200�410Pp*�.�t45�
200F10�r�E��̹b���� ��     